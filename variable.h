#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#define f2      	((float)(1./2.))
#define	f3			((float)(f2/3.))
#define	f4			((float)(f3/4.))
#define	f5			((float)(f4/5.))
#define	f6			((float)(f5/6.))
#define	f7			((float)(f6/7.))
#define	f8			((float)(f7/8.))
#define	f9			((float)(f8/9.))
#define	f10			((float)(f9/10.))
#define	f11			((float)(f10/11.))
#define	f12			((float)(f11/12.))
#define	f13			((float)(f12/13.))
#define	f14			((float)(f13/14.))
#define	f15			((float)(f14/15.))


#define SIN_m(x,x2)		((x)*((float)1.-(x2)*(f3-(x2)*(f5-(x2)*(f7-(x2)*(f9-(x2)*(f11-(x2)*(f13-(x2)*f15))))))))
#define COS_m(x2)			((float)1.-(x2)*(f2-(x2)*(f4-(x2)*(f6-(x2)*(f8-(x2)*(f10-(x2)*(f12-(x2)*f14)))))))



#define 	LIMIT(x,s,l)			(((x)>(l))?(l):((x)<(s))?((s)):(x))
#define		PI                      3.14159265359 
#define		BOUND_PI(x)             ((x>0)?((x)-2.*PI*(int)((x+PI)/(2.*PI))):((x)-2.*PI*(int)((x-PI)/(2.*PI))))
#define     ONE_OVER_ROOT3          0.577350269
#define 	ROOT3               	1.732050808
#define     WPM2WRM                 ((2.*PI)/60.)
//#define SIN(x,x2)		((x)*((float)1.-(x2)*(f3-(x2)*(f5-(x2)*(f7-(x2)*(f9-(x2)*(f11-(x2)*(f13-(x2)*f15))))))))
//#define COS(x2)			((float)1.-(x2)*(f2-(x2)*(f4-(x2)*(f6-(x2)*(f8-(x2)*(f10-(x2)*(f12-(x2)*f14)))))))
//#define SIN_INV_X(x2)   (((float)1.-(x2)*(f3-(x2)*(f5-(x2)*(f7-(x2)*(f9-(x2)*(f11-(x2)*(f13-(x2)*f15))))))))
//#define EXP(x)			((float)1.+(x)*((float)1.+(x)*(f2+(x)*(f3+(x)*(f4+(x)*(f5+(x)*(f6+(x)*f7)))))))
#define	SQRT2		((float)1.414213562)
#define	SQRT3		((float)1.732050808)
#define	INV_SQRT3	((float)0.577350269)
#define	INV_SQRT2	((float)1./SQRT2)
#define	INV_3		((float)0.333333333)
#define INV_2_3		((float)0.666666667)




#define	K_ALLPASS	0
#define	K_LPF		1
#define	K_HPF		2
#define	K_BPF		3
#define	K_NOTCH		4

#define Rm2Rpm		((float)9.5492965855137201461330258023509)	
#define Rpm2Rm		((float)0.10471975511965977461542144610932)

#define	K_ALLPASS	0
#define	K_LPF		1
#define	K_HPF		2
#define	K_BPF		3
#define	K_NOTCH		4






//system variable
extern float time;
extern float Tsamp;
extern float Tsamp1;
extern float Tsamp2;
extern int ini_check;

//Mode switch variable
extern int PC_on;
extern int PC_ref_shaping;
extern int SC_on;
extern int SC_ref_shaping;
extern int CC_on;
extern int CC_ref_shaping;
extern int PI_on;
extern int cc_mode_number;
enum cc_mode{ PICtrl, DeadbeatCtrl, DTCCtrl, NewDTC, DTCPI };
enum FT_mode{ FsTs, FlTs, FsTl, FlTl };
extern int predict_on;
extern int variable_time_on;

//Control variable
extern float a;
extern float b;

extern float CosTheta;
extern float SinTheta;

extern float l1, l2, l3;
extern float theta_unBounded;

extern float Td;
//variable sampling
extern int variable_on;
extern int boost_cnt;
extern int pc_mode;

//etc

extern float acc;
extern float Theta_predict;


// Motor Parameter

typedef struct _Para_Motor_
{
	int Pole, PolePair;
	float InvPolePair;
	//float Rs_tot, Ls_tot, Ke_tot, Lds_tot, Lqs_tot, LAMpm_tot, INV_LAMpm_tot, Kt_tot, INV_Kt_tot;
	float Rs, Ls, Ke, Lds, Lqs, INV_Ke;
	float Rs_real, Ls_real, Ke_real, Lds_real, Lqs_real, INV_Ke_real;

	float Te_rated, Is_rated, Pos_Jerk_rated, Neg_Jerk_rated;
	float Pout_rated;
	float Jm, Bm, InvJm;
	float Kt, INV_Kt;
	float Idss, Iqss, Idse, Iqse;
	float Theta_re, Theta_rm, Theta_rm_load;
	float Thetam_ref, Thetam_ref_set, Thetam_Err_diff, Thetam_Err_integ, Thetam_Err, Thetam_Err_prev, Thetam_added;
	float Thetam_temp, Thetam_prev, Thetam_diff;
	float Wr, Wrm, Wrpm;
	float Wrm_ref, Wrm_ref_set, Wrm_ref_prev, Wrm_ref_diff, Wrpm_ref, Wrm_ref_ff;
	float Wrm_Err, Wrm_rated, Wrm_slope_rate, Wrm_prev;

	float Te_ref, Te_real, Te_ref_set, acc_ref_ff;
	float Te_integ;

	float CosTheta, SinTheta;

	float alpha;
	float Idse_ref_diff, Idse_slope_rate;
	float Iqse_ref_diff, Iqse_slope_rate;
	float Idse_prev, Iqse_prev;
	float Idse_ref_set, Iqse_ref_set;
	float Idse_ref, Iqse_ref;
	float Idse_ref_prev, Iqse_ref_prev;
	float Idse_ref_prev2, Iqse_ref_prev2;
	float Idse_Err_integ, Iqse_Err_integ;
	float Idse_Err, Iqse_Err;


	float Vdse_ref, Vqse_ref;
	float Vdse_ref_prev, Vqse_ref_prev;
	float Vdss_ref, Vqss_ref;
	float Vdse_fb, Vqse_fb;


	float Van_ref_limited, Vbn_ref_limited, Vcn_ref_limited;
	float Van_diff, Vbn_diff, Vcn_diff;
	float Vdss_anti, Vqss_anti;
	float Vdse_anti, Vqse_anti;

	float Vdss_limited, Vqss_limited, Vdse_limited, Vqse_limited;

	float Vdse_ref_set, Vqse_ref_set;
	float Vdss_ref_set, Vqss_ref_set;
	float Vas_ref, Vbs_ref, Vcs_ref;
	float Van_ref, Vbn_ref, Vcn_ref;
	float Van_ref_set, Vbn_ref_set, Vcn_ref_set;
	float Vsn;

	float V_max, V_min;

	float Ia, Ib, Ic;
	float Vdc, half_Vdc;


	float Ki_sc, Kp_sc, Ki_scT, Ka_sc, zeta_sc;
	float Kpd_cc, Kid_cc, Kid_ccT, Kad_cc;
	float Kpq_cc, Kiq_cc, Kiq_ccT, Kaq_cc;
	float Wcc;
	float Wsc;
	float Wpc;
	float Kp_pc, Ki_pc, Ki_pcT, Kd_pc, Kd_pcT;


	float Vdse_ff, Vqse_ff;

	float Vas_ref_set, Vcs_ref_set, Vbs_ref_set;


} Motor;

Motor Inv1;

// Speed Observer
typedef struct _SPD_OBS
{
	float	Theta_est, Err_Theta, Err_Theta_integ;
	float	Te_est_old, Te_est, Tl_est, Te_est_filter, Teffect;
	float	Acc_integ;
	float	Ki_so, Kp_so, Kd_so, Wc_so;			//Wc_so : observer bandwidth(beta)
	float	Wr_est, Wr_est_d, Wrm_est, Wrpm_est;
	float	Wc_filter;
	float	alpha_ff;
	float	Wrm_est_d, Tl_est_d;
	float   Theta_est_d, Theta_r_est, Theta_rm_est;

} SpdObs;
SpdObs Obs1;

typedef struct _DIST_OBS
{
	float Wdis;
	float bf_filt;
	float af_filt;
	float af_filt_prev;
	float DisTorque;
	float senced;
	float Torque_ref;

} DstObs;
DstObs Obsd;

typedef struct _TDOF
{
	float Wrm_Err;
	float Wrm_ref;
	float Wrm, Wrm_est;
	float Kp_sc, Ka_sc;
	float Ki_sc, Ki_scT;
	float Te_ref, Te_integ;
	float Td_est, Td_integ;
	float Teff, Teff_integ;

	float Iqse_ref_set;
	float Iqse_ref;
	float Iqse_ref_prev;

	float Kp_m;
	float Ki_m, Ki_mT;
	
} TDOF;
TDOF TDOFV;


typedef struct	{
	int type;
	float w0;
	float delT;
	float coeff[3], reg;
}	IIR1;
IIR1 LPF;

typedef struct	{
	int type;
	float w0, zeta;
	float delT;
	float coeff[5], reg[2];
}	IIR2;

IIR2 NOTCH_1, NOTCH_2, NOTCH_3, NOTCH_4;
IIR2 NOTCH_C_1, NOTCH_C_2;
IIR2 BPF_1, BPF_2;
IIR1 LPF_1, LPF_2, LPF_3, LPF_4, LPF_5, LPF_6, LPF_7, LPF_8, LPF_9, LPF_10, LPF_11, LPF_12, LPF_13, LPF_14, LPF_15;
IIR1 HPF_1, HPF_2;


void initiateIIR1(IIR1 *p_gIIR, int type, float w0, float Tsamp);
void initiateIIR2(IIR2 *p_gIIR, int type, float w0, float zeta, float Tsamp);

float IIR1Update(IIR1 *p_gIIR, float input);
float IIR2Update(IIR2 *p_gIIR, const float input);

void InitParameters(void);
void InitController(void);
void UpdatePara();
void SpeedObserver(Motor *tmpM, SpdObs *tmpO);
void UpdateGainObs(Motor *tmpM, SpdObs *tmpO);
void InitObs(SpdObs *tmpO, float Wc);
void UpdatePara();
void InitParameters(void);
void InitController(void);
float IIR2Update(IIR2 *p_gIIR, const float x);
void initiateIIR2(IIR2 *p_gIIR, int type, float w0, float zeta, float Tsamp);
float IIR1Update(IIR1 *p_gIIR, float x);
void initiateIIR1(IIR1 *p_gIIR, int type, float w0, float Tsamp);


void PC_ref_calib(void);
void PC_pdctrl(void);
void SC_pictrl(void);
void CC_pictrl(void);
float Thetar_added(float thetar);
void InitDistObs(DstObs *tmpD, float Wc);
void DisturbObserver(Motor *tmpM, DstObs *tmpD);
void TDOFcontrol(void);


#endif//	__CONTROLLER_H__