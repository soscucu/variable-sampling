#include "DllHeader.h"
#include "variable.h"

//Global variables


DLLEXPORT void plecsSetSizes(struct SimulationSizes* aSizes)
{
   aSizes->numInputs = 12;
   aSizes->numOutputs = 17; 
   aSizes->numParameters = 1; //number of user parameters passed in

}

   
//This function is automatically called at the beginning of the simulation
DLLEXPORT void plecsStart(struct SimulationState* aState)
{
	Tsamp = aState->parameters[0];
}
float temp = 0.;

//This function is automatically called every sample time
//output is written to DLL output port after the output delay
DLLEXPORT void plecsOutput(struct SimulationState* aState)
{

	Inv1.Vdc = aState->inputs[0];
	Inv1.Ia = aState->inputs[1];
	Inv1.Ib = aState->inputs[2];
	Inv1.Ic = aState->inputs[3];
	Inv1.Thetam_ref_set = aState->inputs[4];
	Inv1.Theta_rm = aState->inputs[5];
	Inv1.Theta_rm_load = aState->inputs[6];
	Inv1.Wpc = aState->inputs[7];
	Inv1.Wsc = aState->inputs[8];
	Inv1.Wcc = aState->inputs[9];
	PI_on = aState->inputs[10];

	if (ini_check == 0){
		InitParameters();
		InitController();
		InitObs(&Obs1, 2 * PI * 30.);
		InitDistObs(&Obsd, 2 * PI * 500.);
		InitTDOF(&TDOFV, 2 * PI*0.1);
	//	Obsd.Wdis = 2 * PI * 500.;
	}
	
	UpdatePara();
	UpdateGainObs(&Inv1, &Obs1);

	Inv1.half_Vdc = 0.5 * Inv1.Vdc;

	/////////////Angle Generate//////////////
	theta_unBounded = Inv1.Theta_rm;
	Inv1.Theta_rm = BOUND_PI(Inv1.Theta_rm);

	Inv1.Theta_re = BOUND_PI(Inv1.Theta_rm * Inv1.PolePair);
	a = Inv1.Theta_re*Inv1.Theta_re;
	Inv1.CosTheta = COS_m(a);
	Inv1.SinTheta = SIN_m(Inv1.Theta_re, a);
	

	Inv1.Te_real = 1.5 * Inv1.PolePair * (Inv1.Ke + 0.0)*Inv1.Iqse;
	Inv1.Wrm_prev = Inv1.Wrm;
	SpeedObserver(&Inv1, &Obs1);
	DisturbObserver(&Inv1, &Obsd);

	///////////// Current Transformation ///////////
	Inv1.Idss = Inv1.Ia * INV_3 * 2 - Inv1.Ib * INV_3 -  Inv1.Ic * INV_3;
	Inv1.Iqss = Inv1.Ib * SQRT3 * INV_3 - Inv1.Ic * SQRT3 * INV_3;

	Inv1.Idse = Inv1.Idss * Inv1.CosTheta + Inv1.Iqss * Inv1.SinTheta;
	Inv1.Iqse = -Inv1.Idss * Inv1.SinTheta + Inv1.Iqss * Inv1.CosTheta;
	
	Inv1.Te_real = 1.5 * Inv1.PolePair * (Inv1.Ke + 0.0)*Inv1.Iqse;
	/////////////// Position Control(PD) ////////////
	acc = Inv1.Te_real / Inv1.Jm;
	Theta_predict = theta_unBounded + Inv1.Wrm*Tsamp + 0.5*acc*Tsamp*Tsamp;

	if (PC_on){
	//	Inv1.Thetam_ref_set = 50.;
		//code are only for Thetam_ref_set is positive step 
		if (PC_ref_shaping)	PC_ref_calib();
		else Inv1.Thetam_ref = Inv1.Thetam_ref_set;
		PC_pdctrl();

	}
	else {
		Inv1.Wrm_ref = Inv1.Wrm_ref_set;
	}
	Inv1.Thetam_prev = theta_unBounded;

	/////////////// Speed Contro(lPI) ////////////
	if (SC_on){
		TDOFcontrol();
		SC_pictrl();

	}

//current controller
	if (CC_on){

		CC_pictrl();

		Inv1.Idse_ref_prev2 = Inv1.Idse_ref_prev;
		Inv1.Iqse_ref_prev2 = Inv1.Iqse_ref_prev;
		Inv1.Idse_ref_prev = Inv1.Idse_ref;
		Inv1.Iqse_ref_prev = Inv1.Iqse_ref;
		Inv1.Vdse_ref_prev = Inv1.Vdse_ref;
		Inv1.Vqse_ref_prev = Inv1.Vqse_ref;
		
		Inv1.Theta_re += Inv1.Wr * 1.5*Tsamp;
		a = Inv1.Theta_re*Inv1.Theta_re;
		Inv1.CosTheta = COS_m(a);
		Inv1.SinTheta = SIN_m(Inv1.Theta_re, a);

		Inv1.Vdss_ref = Inv1.Vdse_ref * Inv1.CosTheta - Inv1.Vqse_ref * Inv1.SinTheta;
		Inv1.Vqss_ref = Inv1.Vdse_ref * Inv1.SinTheta + Inv1.Vqse_ref * Inv1.CosTheta;

		Inv1.Vas_ref = Inv1.Vdss_ref;
		Inv1.Vbs_ref = -0.5 * (Inv1.Vdss_ref - SQRT3 * Inv1.Vqss_ref);
		Inv1.Vcs_ref = -0.5 * (Inv1.Vdss_ref + SQRT3 * Inv1.Vqss_ref);

		if (Inv1.Vas_ref > Inv1.Vbs_ref)
		{
			Inv1.V_max = Inv1.Vas_ref;
			Inv1.V_min = Inv1.Vbs_ref;
		}
		else
		{
			Inv1.V_max = Inv1.Vbs_ref;
			Inv1.V_min = Inv1.Vas_ref;
		}

		if (Inv1.Vcs_ref > Inv1.V_max)	Inv1.V_max = Inv1.Vcs_ref;
		if (Inv1.Vcs_ref < Inv1.V_min)	Inv1.V_min = Inv1.Vcs_ref;

		//SVPWM
		Inv1.Vsn = -(Inv1.V_max + Inv1.V_min) * 0.5;
		
			Inv1.Van_ref = Inv1.Vas_ref + Inv1.Vsn;
			Inv1.Vbn_ref = Inv1.Vbs_ref + Inv1.Vsn;
			Inv1.Vcn_ref = Inv1.Vcs_ref + Inv1.Vsn;

	}

	Inv1.Van_ref_limited = LIMIT(Inv1.Van_ref, -Inv1.Vdc / 2, Inv1.Vdc / 2);
	Inv1.Vbn_ref_limited = LIMIT(Inv1.Vbn_ref, -Inv1.Vdc / 2, Inv1.Vdc / 2);
	Inv1.Vcn_ref_limited = LIMIT(Inv1.Vcn_ref, -Inv1.Vdc / 2, Inv1.Vdc / 2);

	Inv1.Van_diff = Inv1.Van_ref - Inv1.Van_ref_limited;
	Inv1.Vbn_diff = Inv1.Vbn_ref - Inv1.Vbn_ref_limited;
	Inv1.Vcn_diff = Inv1.Vcn_ref - Inv1.Vcn_ref_limited;

	Inv1.Vdss_anti = Inv1.Van_diff * INV_3 * 2 - Inv1.Vbn_diff * INV_3 - Inv1.Vcn_diff * INV_3;
	Inv1.Vqss_anti = Inv1.Vbn_diff * SQRT3 * INV_3 - Inv1.Vcn_diff * SQRT3 * INV_3;

	Inv1.Vdse_anti = Inv1.Vdss_anti * Inv1.CosTheta + Inv1.Vqss_anti * Inv1.SinTheta;
	Inv1.Vqse_anti = -Inv1.Vdss_anti * Inv1.SinTheta + Inv1.Vqss_anti * Inv1.CosTheta;


	Inv1.Vdss_limited = Inv1.Van_ref_limited * INV_3 * 2 - Inv1.Vbn_ref_limited * INV_3 - Inv1.Vcn_ref_limited * INV_3;
	Inv1.Vqss_limited = Inv1.Vbn_ref_limited * SQRT3 * INV_3 - Inv1.Vcn_ref_limited * SQRT3 * INV_3;
	Inv1.Vdse_limited = Inv1.Vdss_limited * Inv1.CosTheta + Inv1.Vqss_limited * Inv1.SinTheta;
	Inv1.Vqse_limited = -Inv1.Vdss_limited * Inv1.SinTheta + Inv1.Vqss_limited * Inv1.CosTheta;
	

	ini_check = 1;


	//For Output define

	aState->outputs[0] = Inv1.Van_ref_limited / Inv1.Vdc;
	aState->outputs[1] = Inv1.Vbn_ref_limited / Inv1.Vdc;
	aState->outputs[2] = Inv1.Vcn_ref_limited / Inv1.Vdc;



	aState->outputs[3] = Inv1.Idse;
	aState->outputs[4] = Inv1.Iqse; 
	aState->outputs[5] = Inv1.Idse_ref; 
	aState->outputs[6] = Inv1.Iqse_ref; 
	aState->outputs[7] = Inv1.Thetam_ref_set; 
	aState->outputs[8] = Inv1.Thetam_ref; 
	aState->outputs[9] = TDOFV.Td_est;
	aState->outputs[10] = Inv1.Wrm_ref_ff;
	aState->outputs[11] = Inv1.Wrm_ref;
	aState->outputs[12] = Inv1.Te_ref; 
	aState->outputs[13] = Obsd.DisTorque; 
	aState->outputs[14] = pc_mode;

}




