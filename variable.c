#include "variable.h"
#include "math.h"
//system variable
float time = 0;
float Tsamp = 1 / 40000.;
float Tsamp_tdof = 1 / 40000.;
int ini_check = 0;

//Mode switch variable
int PC_on = 1;
int PC_ref_shaping = 0;
int SC_on = 1;
int CC_on = 1;
int PI_on = 0;
int cc_mode_number = 0;//0 : PI, 1 : Deadbeat, 2 : DTC, 3 : NewDTC, 4 : DTC+PI
int predict_on = 0; //Deadbeat should have predict_on = 0
int variable_time_on = 0;
int sampling_boost = 0;
int prediction_angle_on = 0;


//Control variable
float a = 0;
float b = 0;

float CosTheta = 0;
float SinTheta = 0;

float l1, l2, l3;
float theta_unBounded = 0;
int pc_mode = 0;

//variable sampling
int variable_on = 0;
int boost_cnt = 0;

//etc

float acc = 0.;
float Theta_predict = 0.;
float Td = 0.;
void initiateIIR1(IIR1 *p_gIIR, int type, float w0, float Tsamp)
{
	float a0, b0, b1;
	float INV_alpha;

	// Continuous-time Filter Coefficients
	p_gIIR->w0 = w0;
	p_gIIR->type = type;
	p_gIIR->delT = Tsamp;

	a0 = w0;
	switch (type)
	{
	case K_LPF:
		b0 = w0;
		b1 = 0;
		break;
	case K_HPF:
		b0 = 0;
		b1 = (float)1;
		break;
	default:
	case K_ALLPASS:
		b0 = -w0;
		b1 = (float)1;
	}

	// Discrete-time Filter CoefficienTsamp
	INV_alpha = (float)1. / ((float)2 + Tsamp*a0);
	p_gIIR->coeff[0] = ((float)2 * b1 + Tsamp*b0)*INV_alpha;
	p_gIIR->coeff[1] = (-(float)2 * b1 + Tsamp*b0)*INV_alpha;
	p_gIIR->coeff[2] = -(-(float)2 + Tsamp*a0)*INV_alpha;
	p_gIIR->reg = 0.;
}

float IIR1Update(IIR1 *p_gIIR, float x)
{
	float y;

	y = p_gIIR->reg + p_gIIR->coeff[0] * x;
	p_gIIR->reg = p_gIIR->coeff[1] * x + p_gIIR->coeff[2] * y;

	return(y);
}

void initiateIIR2(IIR2 *p_gIIR, int type, float w0, float zeta, float Tsamp)
{
	float a0, a1, b0, b1, b2;
	float INV_alpha;

	// Continuous-time Filter Coefficients
	p_gIIR->w0 = w0;
	p_gIIR->zeta = zeta;
	p_gIIR->delT = Tsamp;
	p_gIIR->type = type;

	a0 = w0*w0;
	a1 = 2 * zeta*w0;
	switch (type)
	{
	case K_LPF:
		b0 = w0*w0;
		b1 = 0;
		b2 = 0;
		break;
	case K_HPF:
		b0 = 0;
		b1 = 0;
		b2 = (float)1;
		break;
	case K_BPF:
		b0 = 0;
		b1 = (float)2 * zeta*w0;
		b2 = 0;
		break;
	case K_NOTCH:
		b0 = w0*w0;
		b1 = 0;
		b2 = (float)1;
		break;
	case K_ALLPASS:
	default:
		b0 = w0*w0;
		b1 = -(float)2 * zeta*w0;
		b2 = (float)1;
	}

	// Discrete-time Filter Coefficients
	INV_alpha = (float)1. / ((float)4 + (float)2 * Tsamp*a1 + Tsamp*Tsamp*a0);
	p_gIIR->coeff[0] = ((float)4 * b2 + (float)2 * Tsamp*b1 + Tsamp*Tsamp*b0)*INV_alpha;
	p_gIIR->coeff[1] = ((float)2 * Tsamp*Tsamp*b0 - (float)8 * b2)*INV_alpha;
	p_gIIR->coeff[2] = -((float)2 * Tsamp*Tsamp*a0 - (float)8)*INV_alpha;
	p_gIIR->coeff[3] = ((float)4 * b2 - (float)2 * Tsamp*b1 + Tsamp*Tsamp*b0)*INV_alpha;
	p_gIIR->coeff[4] = -((float)4 - (float)2 * Tsamp*a1 + Tsamp*Tsamp*a0)*INV_alpha;

	p_gIIR->reg[0] = 0;
	p_gIIR->reg[1] = 0;

}

float IIR2Update(IIR2 *p_gIIR, const float x)
{
	float y;

	y = p_gIIR->reg[0] + p_gIIR->coeff[0] * x;
	p_gIIR->reg[0] = p_gIIR->reg[1] + p_gIIR->coeff[1] * x + p_gIIR->coeff[2] * y;
	p_gIIR->reg[1] = p_gIIR->coeff[3] * x + p_gIIR->coeff[4] * y;

	return(y);
}

void InitController(void)
{


	///position controller parameter///
	Inv1.Kp_pc = Inv1.Wpc;
	Inv1.Ki_pc = 0;
	Inv1.Ki_pcT = Inv1.Ki_pc * Tsamp;
	Inv1.Kd_pc = Inv1.Wpc / Inv1.Wsc;
	Inv1.Kd_pcT = Inv1.Kd_pc / Tsamp;

	///speed controller parameter///
	Inv1.zeta_sc = 1.;
	Inv1.Kp_sc = 2 * Inv1.zeta_sc*Inv1.Wsc*Inv1.INV_Kt*Inv1.Jm;
	Inv1.Ki_sc = Inv1.Wsc*Inv1.Wsc*Inv1.INV_Kt*Inv1.Jm;
//	Inv1.Kp_sc = Inv1.Wsc*Inv1.Jm*Inv1.INV_Kt;
//	Inv1.Ki_sc = Inv1.Wsc*Inv1.Bm*Inv1.INV_Kt;
	Inv1.Ki_scT = Inv1.Ki_sc * Tsamp;

	Inv1.Ka_sc = 1 / Inv1.Kp_sc;
	Inv1.alpha = 1;
	
	///current controller parameter///
	Inv1.Kpd_cc = Inv1.Lds * Inv1.Wcc;
	Inv1.Kid_cc = Inv1.Rs  * Inv1.Wcc;
	Inv1.Kid_ccT = Inv1.Kid_cc * Tsamp;

	Inv1.Kpq_cc = Inv1.Lqs * Inv1.Wcc;
	Inv1.Kiq_cc = Inv1.Rs * Inv1.Wcc;
	Inv1.Kiq_ccT = Inv1.Kiq_cc * Tsamp;

	Inv1.Kad_cc = 1 / Inv1.Kpd_cc;
	Inv1.Kaq_cc = 1 / Inv1.Kpq_cc;

	Inv1.Vdse_ref = 0;
	Inv1.Vdse_ref_set = 0.;
	Inv1.Vqse_ref = 0.;
	Inv1.Vqse_ref_set = 0.;

	Inv1.Idse_ref = 0.;
	Inv1.Iqse_ref = 0.;
	Inv1.Idse_ref = 0.;
	Inv1.Iqse_ref = 0.;
	Inv1.Idse_Err = 0.;
	Inv1.Iqse_Err = 0.;


	initiateIIR2(&HPF_1, K_HPF, 2 * PI * 20, 0.8, Tsamp);
}


void InitParameters(void){

	Inv1.Pout_rated = 11000;
	Inv1.Is_rated = 5;
	Inv1.Wrm_rated = 314.;

	Inv1.Rs_real = 0.35625;
	Inv1.Ls_real = 0.008;
	Inv1.Lds_real = Inv1.Ls;
	Inv1.Lqs_real = Inv1.Ls;
	Inv1.Ke_real = 0.0808;
	Inv1.INV_Ke_real = 1. / Inv1.Ke;

	Inv1.Pole = 8;
	Inv1.PolePair = Inv1.Pole*0.5;
	Inv1.InvPolePair = 1. / Inv1.PolePair;

	Inv1.Rs = Inv1.Rs_real;
	Inv1.Ls = Inv1.Ls_real;
	Inv1.Lds = Inv1.Ls;
	Inv1.Lqs = Inv1.Ls;
	Inv1.Ke = Inv1.Ke_real;
	Inv1.INV_Ke = 1. / Inv1.Ke;



	Inv1.Kt = Inv1.Ke * 1.5 * Inv1.PolePair;
	Inv1.INV_Kt = 1. / Inv1.Kt;



	Inv1.Jm = 0.000765;
	Inv1.InvJm = 1 / Inv1.Jm;
	Inv1.Bm = 0.001;
		
	Inv1.Te_rated = Inv1.Is_rated * Inv1.Kt;
	Inv1.Wrm_slope_rate = Inv1.Kt*Inv1.InvJm*Inv1.Is_rated;
	Inv1.Idse_slope_rate = Inv1.Vdc * 2 / 3 / Inv1.Lds;
	Inv1.Iqse_slope_rate = Inv1.Vdc * 2 / 3 / Inv1.Lds;
	
	Inv1.Wr = 0;
	Inv1.Wrm = 0;


	Inv1.Wrm_ref = 0;
	Inv1.Wrm_Err = 0;
	Inv1.Wrm_prev = 0.;
	Inv1.Te_real = 0;
	Inv1.Te_integ = 0;
	Inv1.Idse_Err_integ = 0;
	Inv1.Iqse_Err_integ = 0;

}

int cnt = 0;
void UpdatePara(){

	

	///position controller parameter///
	Inv1.Kp_pc = Inv1.Wpc;
	Inv1.Ki_pc = 0.;
	Inv1.Ki_pcT = Inv1.Ki_pc * Tsamp;
	Inv1.Kd_pc = Inv1.Wpc / Inv1.Wsc;
	Inv1.Kd_pcT = Inv1.Kd_pc / Tsamp;

	///speed controller parameter///

	Inv1.zeta_sc = 1.;
	Inv1.Kp_sc = 2 * Inv1.zeta_sc*Inv1.Wsc*Inv1.INV_Kt*Inv1.Jm;
	Inv1.Ki_sc = Inv1.Wsc*Inv1.Wsc*Inv1.INV_Kt*Inv1.Jm;
	Inv1.Ki_scT = Inv1.Ki_sc * Tsamp;

	Inv1.Ka_sc = 1 / Inv1.Kp_sc;
	Inv1.alpha = 1;

	///current controller parameter///
	Inv1.Kpd_cc = Inv1.Lds * Inv1.Wcc;
	Inv1.Kid_cc = Inv1.Rs  * Inv1.Wcc;
	Inv1.Kid_ccT = Inv1.Kid_cc * Tsamp;

	Inv1.Kpq_cc = Inv1.Lqs * Inv1.Wcc;
	Inv1.Kiq_cc = Inv1.Rs * Inv1.Wcc;
	Inv1.Kiq_ccT = Inv1.Kiq_cc * Tsamp;

	Inv1.Kad_cc = 1 / Inv1.Kpd_cc;
	Inv1.Kaq_cc = 1 / Inv1.Kpq_cc;

}



void InitObs(SpdObs *tmpO, float Wc)
{
	tmpO->Theta_est = 0., tmpO->Err_Theta = 0., tmpO->Err_Theta_integ = 0.;
	tmpO->Te_est = 0., tmpO->Te_est_old = 0., tmpO->Tl_est = 0.;
	tmpO->Acc_integ = 0.;
	tmpO->Wr_est = 0., tmpO->Wr_est_d = 0.;
	tmpO->Wrm_est = 0., tmpO->Wrpm_est = 0.;
	tmpO->Wc_so = Wc;
	tmpO->Wc_filter = 300.;
	tmpO->alpha_ff = 0.;
	tmpO->Theta_est_d = 0.;
	tmpO->Wrm_est_d = 0.;
	tmpO->Tl_est_d = 0.;
	tmpO->Theta_r_est = 0.;
}

void UpdateGainObs(Motor *tmpM, SpdObs *tmpO)
{
	int beta = -1.0*tmpO->Wc_so;

	l1 = -3.0*beta;
	l2 = 3.0*beta*beta;
	l3 = beta*beta*beta*tmpM->Jm;

	tmpO->Kp_so = l2*tmpM->Jm;
	tmpO->Ki_so = -1.0*l3;
	tmpO->Kd_so = l1;
}
void SpeedObserver(Motor *tmpM, SpdObs *tmpO)
{

	tmpO->Err_Theta = BOUND_PI(tmpM->Theta_rm - tmpO->Theta_rm_est);
	tmpO->Err_Theta_integ += Tsamp * tmpO->Err_Theta;

	tmpO->Tl_est = tmpO->Ki_so * tmpO->Err_Theta_integ;

	tmpO->Acc_integ = tmpM->Te_real + tmpO->Kp_so * tmpO->Err_Theta + tmpO->Tl_est - tmpM->Bm*tmpO->Wrm_est;;
	tmpO->Wrm_est += Tsamp * (tmpO->Acc_integ * tmpM->InvJm);
	tmpO->Wrm_est_d = tmpO->Kd_so * tmpO->Err_Theta;

	tmpO->Theta_rm_est = BOUND_PI(tmpO->Theta_rm_est + Tsamp*(tmpO->Wrm_est + tmpO->Wrm_est_d));
	tmpM->Wrm = tmpO->Wrm_est + tmpO->Wrm_est_d;
	tmpM->Wr = tmpM->Wrm * tmpM->PolePair;
	tmpM->Wrpm = Rm2Rpm * tmpM->Wrm;
}

void InitDistObs(DstObs *tmpD, float Wc)
{
	tmpD->Wdis = Wc;
	tmpD->bf_filt = 0.;
	tmpD->af_filt = 0.;
	tmpD->af_filt_prev = 0.;
	tmpD->DisTorque = 0.;
}

void DisturbObserver(Motor *tmpM, DstObs *tmpD)
{

	//	output = (1 - g*Tsamp)*output_prev + g*Tsamp*input;
	//	output_prev = output;

	tmpD->senced = tmpD->Wdis*tmpM->Jm*Inv1.Wrm;
	tmpD->Torque_ref = tmpM->Iqse*tmpM->Kt;
	tmpD->bf_filt = tmpD->senced + tmpD->Torque_ref;
	tmpD->af_filt = (1 - tmpD->Wdis*Tsamp)*tmpD->af_filt_prev + tmpD->Wdis*Tsamp*tmpD->bf_filt;
	tmpD->af_filt_prev = tmpD->af_filt;
	tmpD->DisTorque = tmpD->af_filt - tmpD->senced;

}
void ReactionObserver(Motor *tmpM, SpdObs *tmpO){
}
void InitTDOF(TDOF *tmpT, float Wt){
/*	Inv1.zeta_sc = 1.8;
	Inv1.Kp_sc = 2 * Inv1.zeta_sc*Inv1.Wsc*Inv1.INV_Kt*Inv1.Jm;
	Inv1.Ki_sc = Inv1.Wsc*Inv1.Wsc*Inv1.INV_Kt*Inv1.Jm;
	Inv1.Ki_scT = Inv1.Ki_sc * Tsamp;

	Inv1.Ka_sc = 1 / Inv1.Kp_sc;
	Inv1.alpha = 1;
*/
	tmpT->Kp_sc = 2 * Inv1.zeta_sc*Inv1.Wsc*Inv1.INV_Kt*Inv1.Jm;
	tmpT->Ka_sc = 1 / tmpT->Kp_sc;
	tmpT->Ki_sc = Inv1.Wsc*Inv1.Wsc*Inv1.INV_Kt*Inv1.Jm;
	tmpT->Ki_scT = tmpT->Ki_sc*Tsamp;

	tmpT->Kp_m = 0.1;
	tmpT->Ki_m = Wt*tmpT->Kp_m;
	tmpT->Ki_mT = tmpT->Ki_m*Tsamp;

	tmpT->Wrm_Err = 0.;
	tmpT->Wrm_ref = 0.;
	tmpT->Wrm = 0.;
	tmpT->Wrm_est = 0.;
	
	tmpT->Te_ref = 0.;
	tmpT->Te_integ = 0.;
	tmpT->Td_est = 0.;
	tmpT->Td_integ = 0.;
	tmpT->Teff = 0.;
	tmpT->Teff_integ = 0.;

	tmpT->Iqse_ref_set = 0.;
	tmpT->Iqse_ref = 0.;


}

void TDOFcontrol(){
	float alpha = 1.0;
	Inv1.Wrm_Err = Inv1.Wrm_ref - Inv1.Wrm;
	//Inv1.Ka_sc = 0.;
	Inv1.Te_integ += Inv1.Ki_scT * (Inv1.Wrm_Err - Inv1.Ka_sc * (Inv1.Te_ref - Inv1.Iqse_ref_prev * Inv1.Kt));
	Inv1.Te_ref = Inv1.acc_ref_ff*Inv1.Jm + Inv1.Kp_sc*Inv1.Wrm_Err + Inv1.Te_integ - 10 * Obsd.DisTorque;// +Td;// +(Obsd.DisTorque - 0.01); // + TDOFV.Te_ref;


	Inv1.Iqse_ref_set = Inv1.Te_ref * Inv1.INV_Kt;
	Inv1.Iqse_ref = ((Inv1.Iqse_ref_set > Inv1.Is_rated) ? Inv1.Is_rated : ((Inv1.Iqse_ref_set < -Inv1.Is_rated) ? -Inv1.Is_rated : Inv1.Iqse_ref_set));
	Inv1.Idse_ref = 0;



	TDOFV.Wrm_ref = Inv1.Wrm_ref;
	TDOFV.Wrm_Err = TDOFV.Wrm_ref - TDOFV.Wrm_est;
	TDOFV.Te_integ += TDOFV.Ki_scT * (TDOFV.Wrm_Err - TDOFV.Ka_sc *(TDOFV.Te_ref - TDOFV.Iqse_ref_prev * Inv1.Kt));
	TDOFV.Te_ref = TDOFV.Kp_sc * TDOFV.Wrm_Err + Inv1.Te_integ;

	TDOFV.Iqse_ref_set = TDOFV.Te_ref * Inv1.INV_Kt;
	TDOFV.Iqse_ref = ((TDOFV.Iqse_ref_set > Inv1.Is_rated) ? Inv1.Is_rated : ((TDOFV.Iqse_ref_set < -Inv1.Is_rated) ? -Inv1.Is_rated : TDOFV.Iqse_ref_set));
	TDOFV.Iqse_ref_prev = TDOFV.Iqse_ref;

	TDOFV.Wrm_Err = TDOFV.Wrm_est - Inv1.Wrm;
	TDOFV.Td_integ += TDOFV.Ki_mT * TDOFV.Wrm_Err;
	TDOFV.Td_est = TDOFV.Kp_m * TDOFV.Wrm_Err + TDOFV.Td_integ;

	TDOFV.Teff = TDOFV.Te_ref - TDOFV.Td_est;
	TDOFV.Teff_integ += TDOFV.Teff*Tsamp;
	TDOFV.Wrm_est = TDOFV.Teff_integ * Inv1.InvJm * alpha;
}

void PC_ref_calib(void){
	switch (pc_mode){
	case 0:	//up 2nd order
		time += Tsamp;
		Inv1.acc_ref_ff = Inv1.Is_rated*Inv1.Kt*Inv1.InvJm;
		Inv1.Wrm_ref_ff += Inv1.acc_ref_ff*Tsamp;
		Inv1.Thetam_ref += Inv1.Wrm_ref_ff*Tsamp;
		Inv1.Thetam_temp = Inv1.Thetam_ref;
		if (Inv1.Wrm_ref_ff >= Inv1.Wrm_rated) pc_mode = 1;
		else if (Inv1.Thetam_ref >= 0.5* Inv1.Thetam_ref_set) pc_mode = 2;
		break;
	case 1:	//linear mode
		Inv1.acc_ref_ff = 0.;
		Inv1.Wrm_ref_ff = Inv1.Wrm_rated;
		Inv1.Thetam_ref += Inv1.Wrm_ref_ff*Tsamp;
		if (Inv1.Thetam_ref >= (Inv1.Thetam_ref_set - Inv1.Thetam_temp)) pc_mode = 2;
		break;
	case 2:	//down 2nd order
		time -= Tsamp;
		Inv1.acc_ref_ff = -Inv1.Is_rated*Inv1.Kt*Inv1.InvJm;
		Inv1.Wrm_ref_ff += Inv1.acc_ref_ff*Tsamp;
		if (Inv1.Wrm_ref_ff <= 0) Inv1.Wrm_ref_ff = 0;
		Inv1.Thetam_ref += Inv1.Wrm_ref_ff*Tsamp;
		if ((Inv1.Thetam_ref >= Inv1.Thetam_ref_set) || (Inv1.Wrm_ref_ff <= 0)) {
			pc_mode = 3;
		}
		break;
	case 3:
		Inv1.acc_ref_ff = 0;
		Inv1.Wrm_ref_ff = 0;
		Inv1.Thetam_ref = Inv1.Thetam_ref_set;
		break;
	}
}

void PC_pdctrl(){
	if (prediction_angle_on)Inv1.Thetam_Err = Inv1.Thetam_ref - Theta_predict;
	else Inv1.Thetam_Err = Inv1.Thetam_ref - theta_unBounded;
	
	Inv1.Thetam_Err_integ += Inv1.Ki_pcT * (Inv1.Thetam_Err - (1 / Inv1.Kp_pc)*(Inv1.Te_ref_set - Inv1.Te_ref));
	Inv1.Thetam_Err_diff = Inv1.Thetam_Err - Inv1.Thetam_Err_prev;
	if (SC_on){
		Inv1.Wrm_ref_set = Inv1.Wrm_ref_ff + Inv1.Kp_pc*Inv1.Thetam_Err + Inv1.Kd_pcT*Inv1.Thetam_Err_diff;
		Inv1.Thetam_Err_prev = Inv1.Thetam_Err;
		Inv1.Wrm_ref = ((Inv1.Wrm_ref_set > Inv1.Wrm_rated) ? Inv1.Wrm_rated : ((Inv1.Wrm_ref_set < -Inv1.Wrm_rated) ? -Inv1.Wrm_rated : Inv1.Wrm_ref_set));
	}
	else{
		Inv1.Te_ref_set = Inv1.Kp_pc * Inv1.Thetam_Err + Inv1.Kd_pcT*Inv1.Thetam_Err_diff + Inv1.Thetam_Err_integ;
		Inv1.Te_ref = ((Inv1.Te_ref_set > Inv1.Is_rated*Inv1.Kt) ? Inv1.Is_rated*Inv1.Kt : ((Inv1.Iqse_ref_set < -Inv1.Is_rated*Inv1.Kt) ? -Inv1.Is_rated*Inv1.Kt : Inv1.Te_ref_set));
		Inv1.Iqse_ref = Inv1.Te_ref * Inv1.INV_Kt;
	}
	Inv1.Thetam_Err_prev = Inv1.Thetam_Err;


}

void SC_pictrl(){
	Inv1.Wrm_Err = Inv1.Wrm_ref - Inv1.Wrm;
	//Inv1.Ka_sc = 0.;
	Inv1.Te_integ += Inv1.Ki_scT * (Inv1.Wrm_Err - Inv1.Ka_sc * (Inv1.Te_ref - Inv1.Iqse_ref_prev * Inv1.Kt));
	Inv1.Te_ref = Inv1.acc_ref_ff*Inv1.Jm + Inv1.Kp_sc*Inv1.Wrm_Err + Inv1.Te_integ+Obs1.Tl_est;// -Obsd.DisTorque;// +Td;// +(Obsd.DisTorque - 0.01); // + TDOFV.Te_ref;

	Inv1.Iqse_ref_set = Inv1.Te_ref * Inv1.INV_Kt;
	Inv1.Iqse_ref = ((Inv1.Iqse_ref_set > Inv1.Is_rated) ? Inv1.Is_rated : ((Inv1.Iqse_ref_set < -Inv1.Is_rated) ? -Inv1.Is_rated : Inv1.Iqse_ref_set));
	Inv1.Idse_ref = 0;
}

void CC_pictrl(){
	Inv1.Idse_Err = Inv1.Idse_ref - Inv1.Idse;
	Inv1.Iqse_Err = Inv1.Iqse_ref - Inv1.Iqse;
	if (!PI_on){
		Inv1.Kid_ccT = 0.;
		Inv1.Kiq_ccT = 0.;
	}
	
	Inv1.Idse_Err_integ += Inv1.Kid_ccT*(Inv1.Idse_Err - Inv1.Kad_cc * Inv1.Vdse_anti);
	Inv1.Iqse_Err_integ += Inv1.Kiq_ccT*(Inv1.Iqse_Err - Inv1.Kaq_cc * Inv1.Vqse_anti);

	Inv1.Vdse_ff = -Inv1.Iqse * Inv1.Lqs * (Inv1.Wr);
	Inv1.Vqse_ff = (Inv1.Idse * Inv1.Lds + Inv1.Ke)*(Inv1.Wr);

	Inv1.Vdse_ref = Inv1.Kpd_cc * Inv1.Idse_Err + Inv1.Idse_Err_integ + Inv1.Vdse_ff;
	Inv1.Vqse_ref = Inv1.Kpq_cc * Inv1.Iqse_Err + Inv1.Iqse_Err_integ + Inv1.Vqse_ff;

	Inv1.Idse_prev = Inv1.Idse;
	Inv1.Iqse_prev = Inv1.Iqse;
}
